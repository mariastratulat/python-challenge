''' Step 0 for PentaStagiu
    To solve a few problems '''

import re

# P1 -- The length of the word
def first(word):
    if len(word) < 16 and len(word) > 4:
        print 'ok'
    else:
        print 'ko'

first('Costel')
first('Pi')
first('Anul acesta crestem ceapa pe balcon')

# P2 -- Capitalize the first letter of each word
def second(word):
    if word.istitle():
        print word
    else:
        print word.title()

second('maria')
second('ion popescu')
second('Gheorghe vasile')
second('elena Ceausescu')
second('Ilie Calatorul')

# P3 -- First and last element in arrays
def same_element(a, b):
    if len(a) > 0 and len(b) > 0:
        if a[0] == b[0] or a[-1] == b[-1]:
            print 'ok'
        else:
            print 'ko'
    else:
        print 'The arrays should have at least one element'

same_element([], [1])
same_element([1, 2, 3], [1, 5, 8, 9])
same_element([0, 2], [1, 5, 2])
same_element([1, 2], [1, 3,2])
same_element([1, 2, 3], [4, 5, 6])

# P4 -- The string count
def counts(phrase, word):
    m = word.split('e')
    print m
    count = 0
    for elem in phrase:
        if elem in m:
            count += 1
    print count
            
#counts('Pen\Stagiu')
counts('This isPan\taStagiu my phrasePen\taStagiubecauseI want Pan\taStagiu to Pin\taStagiu at pentalog', 'Pen\taStagiu')
    
    
